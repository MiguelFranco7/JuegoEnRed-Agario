#include "stdafx.h"
#include "../EnetWrapper/ServerENet.h"
#include <Windows.h>
#include <iostream>
#include "../../Config/Message.h"
#include "Math.h"

using namespace ENet;
using namespace std;

vector<User*>     listUsers = vector<User*>();
vector<BallLife*> listBallsLife = vector<BallLife*>();

NetMessageUserStartGame messageUserStartGame;
NetMessageSnapShot		messageSnapShot;
NetMessageChangePos		messageChangePos;
NetMessageDeleteUser	messageDeleteUser;

UserInfo     user;
SnapshotInfo snapshotInfo;

vector<UserInfo>   userInfoList;
vector<EntityInfo> entityInfoList;

char buffer[300];
char* buff;
char* buffNewUser;
int sizeMessage = 0;

NetMessage newMessage;
char *newBuffer;

CServerENet* pServer;

// Funciones
void    sendEatenUser(User* eaten, User* winner);
void    sendEatenBallLife(int idBall, int idUser, int weightUser);
void    sendSnapshot(CPeerENet* peer = nullptr);

int _tmain(int argc, _TCHAR* argv[]) {
    pServer = new CServerENet();
    if (pServer->Init(1234, 5)) {

		// Create balls of life
		for (int i = 0; i < MAX_BALLSLIFE; i++) {
			listBallsLife.push_back(new BallLife(Vec2(rand() % RAND_BALLS_X, rand() % RAND_BALLS_Y), i, static_cast<float>(rand() % 255) / 255.f, static_cast<float>(rand() % 255) / 255.f, static_cast<float>(rand() % 255) / 255.f, rand() % VARIATION_BALLS_SIZE + WEIGHT_BALLS_START));
		}

        while (true) {
            std::vector<CPacketENet*>  incommingPackets;
            pServer->Service(incommingPackets, 0);

			for (int i = 0; i < incommingPackets.size(); i++) {
				switch (incommingPackets[i]->GetType()) {
				case CONNECT:
					cout << "Connect" << endl;
					if (listUsers.size() == 0)
						listUsers.push_back(new User(Vec2(0, 0), incommingPackets[i]->GetPeer(), 0));
					else
						listUsers.push_back(new User(Vec2(0, 0), incommingPackets[i]->GetPeer(), listUsers[listUsers.size() - 1]->mId + 1));

					messageUserStartGame = NetMessageUserStartGame(UserStartGameInfo(listUsers[listUsers.size() - 1]->mId));
					
					buff = (char*)malloc(56);
					sizeMessage = messageUserStartGame.serialize(buff);
					pServer->SendData(incommingPackets[i]->GetPeer(), buff, sizeMessage, 0, true);
					free(buff);

					// Le mandamos el estado de la partida
					sendSnapshot(incommingPackets[i]->GetPeer());
					
					break;
				case DATA:
					newBuffer = reinterpret_cast<char *>(incommingPackets[i]->GetData());
					newMessage.deserialize(newBuffer);

					switch (newMessage.mType) {
					case NETMSG_CHANGEPOS: 
						messageChangePos.deserialize(newBuffer);

						for (int i = 0; i < listUsers.size(); i++) {
							if (listUsers[i]->mId == messageChangePos.mData.mId) {
								listUsers[i]->mPos.x = messageChangePos.mData.mX;
								listUsers[i]->mPos.y = messageChangePos.mData.mY;
								break;
							}
						}

						break;
					default:
						cout << "DEFAULT TYPE" << endl;
						break;
					}
					break;
				case DISCONNECT:
					cout << "disconnet" << endl;
					for (int j = 0; j < listUsers.size(); ++j) {
						if (listUsers[j]->mPeer == incommingPackets[i]->GetPeer()) {
							int id = listUsers[j]->mId;
							listUsers.erase(listUsers.begin() + j);

							messageDeleteUser = NetMessageDeleteUser(DeleteUserInfo(id));
							buffNewUser = (char*)malloc(12);
							sizeMessage = messageDeleteUser.serialize(buffNewUser);
							pServer->SendAll(buffNewUser, sizeMessage, 0, true);
							free(buffNewUser);
						}
					}
					break;
				default:
					cout << "default" << endl;
					break;
				}
			}

			// Comprobar colisiones entre usuarios y usuarios con balls
			for (int i = 0; i < listUsers.size(); i++) {
				float x      = listUsers[i]->mPos.x;
				float y      = listUsers[i]->mPos.y;
				int   id	 = listUsers[i]->mId;
				float weight = listUsers[i]->mWeight;

				// Colisiones con usuarios
				for (int j = 0; j < listUsers.size(); j++) {
					float x1      = listUsers[j]->mPos.x;
					float y1      = listUsers[j]->mPos.y;
					int   id1     = listUsers[j]->mId;
					float weight1 = listUsers[j]->mWeight;

					if ((x - x1) * (x - x1) + (y - y1) * (y - y1) < (weight / 2 + weight1 / 2) * (weight / 2 + weight1 / 2)) {
						if (weight < weight1) {
							listUsers[j]->mWeight += listUsers[i]->mWeight;
							sendEatenUser(listUsers[i], listUsers[j]);
							listUsers.erase(listUsers.begin() + i);
						}
					}
				}

				// Colisiones con bolas
				bool eatenBall = false;
				for (int j = 0; j < listBallsLife.size(); j++) {
					float x1      = listBallsLife[j]->mPos.x;
					float y1      = listBallsLife[j]->mPos.y;
					int   id1     = listBallsLife[j]->mId;
					int   weight1 = listBallsLife[j]->mWeight;

					if ((x - x1) * (x - x1) + (y - y1) * (y - y1) < (weight / 2 + weight1 / 2) * (weight / 2 + weight1 / 2)) {
						eatenBall = true;
						listUsers[i]->mWeight += weight1;
						sendEatenBallLife(listBallsLife[j]->mId, id, listUsers[i]->mWeight);
						listBallsLife.erase(listBallsLife.begin() + j);
					}
				}

				// Si se ha comido una bola se a�ade otra rand a la lista
				if (eatenBall) {
					listBallsLife.push_back(new BallLife(Vec2(rand() % RAND_BALLS_X, rand() % RAND_BALLS_Y), listBallsLife.size(), static_cast<float>(rand() % 255) / 255.f, static_cast<float>(rand() % 255) / 255.f, static_cast<float>(rand() % 255) / 255.f, rand() % VARIATION_BALLS_SIZE + WEIGHT_BALLS_START));
				}
			}

			// Le mandamos el estado de la partida
			if (listUsers.size() > 0) {
				sendSnapshot();
			}

			Sleep(10);
        }
    } else {
        fprintf(stdout, "Server could not be initialized.\n");
    }

    return 0;
}

void sendSnapshot(CPeerENet *peer) {
	vector<UserInfo>   userInfoList   = vector<UserInfo>();
	vector<EntityInfo> entityInfoList = vector<EntityInfo>();

	for (User* u : listUsers) {
		UserInfo user;
		user.mId     = u->mId;
		user.mX      =  u->mPos.x;
		user.mY      =  u->mPos.y;
		user.mSpeed  = u->mSpeed;
		user.mWeight = u->mWeight;

		userInfoList.push_back(user);
	}

	for (BallLife* b : listBallsLife) {
		EntityInfo entity;
		entity.mId	   = b->mId;
		entity.mX	   = b->mPos.x;
		entity.mY	   = b->mPos.y;
		entity.mR	   = b->mR;
		entity.mG	   = b->mG;
		entity.mB	   = b->mB;
		entity.mWeight = b->mWeight;

		entityInfoList.push_back(entity);
	}

	SnapshotInfo snapshotInfo;
	std::copy(userInfoList.begin(),   userInfoList.end(), snapshotInfo.mUsersInfo.begin());
	std::copy(entityInfoList.begin(), entityInfoList.end(), snapshotInfo.mBallsInfo.begin());
	snapshotInfo.mNumPlayers   = userInfoList.size();
	snapshotInfo.mNumBallsLife = entityInfoList.size();

	NetMessageSnapShot messageSnapShot = NetMessageSnapShot(snapshotInfo);
	char* buff = (char*)malloc(1800);
	int sizeMessage = messageSnapShot.serialize(buff);

	if (peer) {
		pServer->SendData(peer, buff, sizeMessage, 0, true);
	} else 
		pServer->SendAll(buff, sizeMessage, 0, false);

	free(buff);
}

void sendEatenUser(User* eaten, User* winner) {
	char* buff = (char*)malloc(32);

	UserEatenInfo info(eaten->mId, winner->mId, winner->mWeight);
	NetMessageUserEaten message = NetMessageUserEaten(info);
	int size = message.serialize(buff);

	pServer->SendAll(buff, size, 0, true);
	free(buff);
}

void sendEatenBallLife(int idBall, int idUser, int weightUser) {
	char* buff = (char*)malloc(32);

	BallLifeEatenInfo info(idBall, idUser, weightUser);
	NetMessageBallLifeEaten message = NetMessageBallLifeEaten(info);
	int size = message.serialize(buff);

	pServer->SendAll(buff, size, 0, true);
	free(buff);
}
