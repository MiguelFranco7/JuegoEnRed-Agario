#pragma once

#include <array>
#include "GameConfig.h"

struct EntityInfo {
	float   mX;
	float   mY;
	int     mId;
	float   mR;
	float   mG;
	float   mB;
	int     mWeight;

	EntityInfo() : mX(0), mY(0), mId(-1), mR(0), mG(0), mB(0), mWeight(WEIGHT_BALLS_START) {}
	EntityInfo(float x, float y, float id, float r, float g, float b, int weight) : mX(x), mY(y), mId(id), mR(r), mG(g), mB(b), mWeight(weight) {}
};

struct UserInfo : public EntityInfo {
	float mSpeed;
	
	UserInfo()												: EntityInfo(0, 0, -1, 0, 0, 0, WEIGHT_BALLS_START), mSpeed(0) {}
	UserInfo(float x, float y, int id, float r, float g, float b, int weight, float speed) : EntityInfo(x, y, id, r, g, b, weight), mSpeed(speed) {}
};

struct ChangePosUserInfo {
	float mX;
	float mY;
	int   mId;

	ChangePosUserInfo() : mX(0), mY(0), mId(0) {}
	ChangePosUserInfo(float x, float y, int id) : mX(x), mY(y), mId(id) {}
};

struct NewUserInfo {
	float mX;
	float mY;
	int   mId;

	NewUserInfo() : mX(0), mY(0), mId(0) {}
	NewUserInfo(float x, float y, int id) : mX(x), mY(y), mId(id) {}
};

struct DeleteUserInfo {
	int mId;

	DeleteUserInfo() : mId(-1) {}
	DeleteUserInfo(int id) : mId(id) {}
};

struct UserEatenInfo {
	int mIdUserEaten;
	int mIdUser;
	unsigned int mWeight;

	UserEatenInfo() : mIdUserEaten(-1), mIdUser(-1), mWeight(20) {}
	UserEatenInfo(int idUserEaten, int idUser, int weight) : mIdUserEaten(idUserEaten), mIdUser(idUser), mWeight(weight) {}
};

struct BallLifeEatenInfo {
	int mId;
	unsigned int mIdUser;
	unsigned int mWeight;

	BallLifeEatenInfo() : mId(-1) {}
	BallLifeEatenInfo(int id, int idUser, int weight) : mId(id), mIdUser(idUser), mWeight(weight) {}
};

struct SnapshotInfo {
	std::array<UserInfo,   MAX_PLAYERS>   mUsersInfo;
	std::array<EntityInfo, MAX_BALLSLIFE> mBallsInfo;
	int mNumPlayers;
	int mNumBallsLife;

	SnapshotInfo() {}
};

struct UserStartGameInfo {
	float mX;
	float mY;
	int   mId;
	float mSpeed;
	int   mWeight;

	UserStartGameInfo() : mX(0.f), mY(0.f), mId(-1), mSpeed(1), mWeight(1) {}
	UserStartGameInfo(int id) : mX(0.f), mY(0.f), mId(id), mSpeed(1), mWeight(1) {}
};
