#pragma once

// Users
#define MAX_PLAYERS  10
#define WEIGHT_START 20
#define SPEED_START  10

// Balls
#define VARIATION_BALLS_SIZE 10
#define RAND_BALLS_X		 3500
#define RAND_BALLS_Y		 800
#define WEIGHT_BALLS_START	 7
#define MAX_BALLSLIFE        50
