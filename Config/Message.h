#pragma once

#include "../src/vec2.h"
#include "../src/User.h"
#include "../src/BallLife.h"
#include "../Config/MessagesInformation.h"
#include <vector>

enum NetMessageType {
	NETMSG_CHANGEPOS,
	NETMSG_NEWUSER,
	NETMSG_DELETEUSER,
	NETMSG_SNAPSHOT,
	NETMSG_USEREATEN,
	NETMSG_BALLEATEN,
	NETMSG_USERSTARTGAME
};

struct NetMessage {
	NetMessageType mType;

	virtual int  serialize(char* outBuff); 
	virtual void deserialize(char* inBuff);
};

struct NetMessageChangePos : public NetMessage {
	ChangePosUserInfo mData;

	NetMessageChangePos() {
		mType = NETMSG_CHANGEPOS;
		mData = ChangePosUserInfo();
	}

	NetMessageChangePos(ChangePosUserInfo data) {
		mType = NETMSG_CHANGEPOS;
		mData = data;
	}

	virtual int   serialize(char* outBuff);
	virtual void  deserialize(char* readBuff);
};

struct NetMessageNewUser : public NetMessage {
	NewUserInfo mData;

	NetMessageNewUser() {
		mType = NETMSG_NEWUSER;
		mData = NewUserInfo();
	}

	NetMessageNewUser(NewUserInfo data) {
		mType = NETMSG_NEWUSER;
		mData = data;
	}

	virtual int   serialize(char* outBuff);
	virtual void  deserialize(char* readBuff);
};

struct NetMessageDeleteUser : public NetMessage {
	DeleteUserInfo mData;

	NetMessageDeleteUser() {
		mType = NETMSG_DELETEUSER;
		mData = DeleteUserInfo();
	}

	NetMessageDeleteUser(DeleteUserInfo data) {
		mType = NETMSG_DELETEUSER;
		mData = data;
	}

	virtual int   serialize(char* outBuff);
	virtual void  deserialize(char* readBuff);
};

struct NetMessageUserEaten : public NetMessage {
	UserEatenInfo mData;

	NetMessageUserEaten() {
		mType = NETMSG_USEREATEN;
		mData = UserEatenInfo();
	}

	NetMessageUserEaten(UserEatenInfo data) {
		mType = NETMSG_USEREATEN;
		mData = data;
	}

	virtual int   serialize(char* outBuff);
	virtual void  deserialize(char* readBuff);
};

struct NetMessageBallLifeEaten : public NetMessage {
	BallLifeEatenInfo mData;

	NetMessageBallLifeEaten() {
		mType = NETMSG_BALLEATEN;
		mData = BallLifeEatenInfo();
	}

	NetMessageBallLifeEaten(BallLifeEatenInfo data) {
		mType = NETMSG_BALLEATEN;
		mData = data;
	}

	virtual int   serialize(char* outBuff);
	virtual void  deserialize(char* readBuff);
};

struct NetMessageSnapShot : public NetMessage {
	SnapshotInfo mData;

	NetMessageSnapShot() {
		mType = NETMSG_SNAPSHOT;
		mData = SnapshotInfo();
	}

	NetMessageSnapShot(SnapshotInfo data) {
		mType = NETMSG_SNAPSHOT;
		mData = data;
	}

	virtual int   serialize(char* outBuff);
	virtual void  deserialize(char* readBuff);
};

struct NetMessageUserStartGame : public NetMessage {
	UserStartGameInfo mData;

	NetMessageUserStartGame() {
		mType = NETMSG_USERSTARTGAME;
		mData = UserStartGameInfo();
	}

	NetMessageUserStartGame(UserStartGameInfo data) {
		mType = NETMSG_USERSTARTGAME;
		mData = data;
	}

	virtual int   serialize(char* outBuff);
	virtual void  deserialize(char* readBuff);
};
