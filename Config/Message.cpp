#include "Message.h"

int NetMessage::serialize(char * outBuff) {
	*reinterpret_cast< NetMessageType*>(outBuff) = mType;
	return sizeof(NetMessage);
}

void NetMessage::deserialize(char * inBuff) {
	mType = *reinterpret_cast<NetMessageType*>(inBuff);
}

int NetMessageSnapShot::serialize(char * outBuff)
{
	int iBytesSerial = NetMessage::serialize(outBuff);
	outBuff += iBytesSerial;

	*reinterpret_cast<SnapshotInfo*>(outBuff) = mData;
	iBytesSerial += sizeof(mData);

	return iBytesSerial;
}

void NetMessageSnapShot::deserialize(char * readBuff)
{
	NetMessage::deserialize(readBuff);
	NetMessageSnapShot m = *reinterpret_cast<NetMessageSnapShot*>(readBuff);
	mData = m.mData;
}

int NetMessageChangePos::serialize(char * outBuff) {
	int iBytesSerial = NetMessage::serialize(outBuff);
	outBuff += iBytesSerial;

	*reinterpret_cast<ChangePosUserInfo*>(outBuff) = mData;
	iBytesSerial += sizeof(ChangePosUserInfo);

	return iBytesSerial;
}

void NetMessageChangePos::deserialize(char * readBuff) {
	NetMessage::deserialize(readBuff);
	NetMessageChangePos m = *reinterpret_cast<NetMessageChangePos*>(readBuff);
	mData = m.mData;
}

int NetMessageNewUser::serialize(char * outBuff) {
	int iBytesSerial = NetMessage::serialize(outBuff);
	outBuff += iBytesSerial;

	*reinterpret_cast<NewUserInfo*>(outBuff) = mData;
	iBytesSerial += sizeof(NewUserInfo);

	return iBytesSerial;
}

void NetMessageNewUser::deserialize(char * readBuff) {
	NetMessage::deserialize(readBuff);
	NetMessageNewUser m = *reinterpret_cast<NetMessageNewUser*>(readBuff);
	mData = m.mData;
}

int NetMessageDeleteUser::serialize(char * outBuff) {
	int iBytesSerial = NetMessage::serialize(outBuff);
	outBuff += iBytesSerial;

	*reinterpret_cast<DeleteUserInfo*>(outBuff) = mData;
	iBytesSerial += sizeof(DeleteUserInfo);

	return iBytesSerial;
}

void NetMessageDeleteUser::deserialize(char * readBuff) {
	NetMessage::deserialize(readBuff);
	NetMessageDeleteUser m = *reinterpret_cast<NetMessageDeleteUser*>(readBuff);
	mData = m.mData;
}

int NetMessageUserEaten::serialize(char * outBuff) {
	int iBytesSerial = NetMessage::serialize(outBuff);
	outBuff += iBytesSerial;

	*reinterpret_cast<UserEatenInfo*>(outBuff) = mData;
	iBytesSerial += sizeof(UserEatenInfo);

	return iBytesSerial;
}

void NetMessageUserEaten::deserialize(char * readBuff) {
	NetMessage::deserialize(readBuff);
	NetMessageUserEaten m = *reinterpret_cast<NetMessageUserEaten*>(readBuff);
	mData = m.mData;
}

int NetMessageBallLifeEaten::serialize(char * outBuff)
{
	int iBytesSerial = NetMessage::serialize(outBuff);
	outBuff += iBytesSerial;

	*reinterpret_cast<BallLifeEatenInfo*>(outBuff) = mData;
	iBytesSerial += sizeof(BallLifeEatenInfo);

	return iBytesSerial;
}

void NetMessageBallLifeEaten::deserialize(char * readBuff) {
	NetMessage::deserialize(readBuff);
	NetMessageBallLifeEaten m = *reinterpret_cast<NetMessageBallLifeEaten*>(readBuff);
	mData = m.mData;
}

int NetMessageUserStartGame::serialize(char * outBuff) {
	int iBytesSerial = NetMessage::serialize(outBuff);
	outBuff += iBytesSerial;

	*reinterpret_cast<UserStartGameInfo*>(outBuff) = mData;
	iBytesSerial += sizeof(UserStartGameInfo);

	return iBytesSerial;
}

void NetMessageUserStartGame::deserialize(char * readBuff) {
	NetMessage::deserialize(readBuff);
	NetMessageUserStartGame m = *reinterpret_cast<NetMessageUserStartGame*>(readBuff);
	mData = m.mData;
}
