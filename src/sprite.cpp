#include "sprite.h"
#include <iostream>

void Sprite::update(float deltaTime) {
	// Llamamos callback
	if (mCallback) mCallback(*this, deltaTime);

	// Actualizamos animacion
	int totalFrames = mHframes * mVframes;
	mCurrentFrame += mFps * deltaTime;
	if (mCurrentFrame >= totalFrames) mCurrentFrame -= totalFrames;
	if (mCurrentFrame < 0) mCurrentFrame += totalFrames;
}

void Sprite::draw() const {
	// Obtenemos coordenadas UV
	int row = static_cast<int>(mCurrentFrame) / mHframes;
	int col = static_cast<int>(mCurrentFrame) % mHframes;
	float stepU = 1.0f / mHframes;
	float stepV = 1.0f / mVframes;
	float u0 = col * stepU;
	float v0 = row * stepV;
	float u1 = u0 + stepU;
	float v1 = v0 + stepV;

	// Establecemos propiedades de pintado
	lgfx_setblend(mBlend);
	lgfx_setcolor(mColor[0], mColor[1], mColor[2], mColor[3]);

	// Pintamos
	Vec2 size = getSize();
	/*ltex_drawrotsized(mTexture,
		mPosition.x, mPosition.y,
		mAngle, mPivot.x, mPivot.y,
		size.x, size.y,
		u0, v0, u1, v1);*/

	lgfx_drawoval(mPosition.x - mWeight / 2, mPosition.y - mWeight / 2, mWeight, mWeight);
}
