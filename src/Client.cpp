#ifdef _MSC_VER
//#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
#endif

#include "stdafx.h"
#include "ugine.h"
#include "../Config/Message.h"
#include <time.h>
#include <glfw3.h>
#include <iostream>
#include <Windows.h>

using namespace ENet;
using namespace std;

struct Client {
	Client(int id) : mId(id), mPos(Vec2(0.f, 0.f)) {}

	Vec2	   mPos;
	int		   mId;
	int		   mWeight = 10;
	float	   mSpeed  = 10;
};

vector<Client*>  listUsers;
vector<BallLife> listBallsLife;
Client			*mPlayer;

void updateBee(Sprite& sprite, float deltaTime);

int main(int argc, _TCHAR* argv[]) {
	// Inicializamos Cliente
    CClienteENet* pClient = new CClienteENet();
    pClient->Init();

	uint32_t seed = static_cast<uint32_t>(time(0));
	srand(seed);

	Sleep(25);
    CPeerENet* pPeer = pClient->Connect("127.0.0.1", 1234, 2);
	mPlayer = new Client(-1);

    std::vector<CPacketENet*>  incommingPackets;
    Sleep(25);

	// Inicializamos el pintado
	// Inicializamos GLFW
	if (!glfwInit()) {
		cout << "Error: No se ha podido inicializar GLFW" << endl;
		return -1;
	}
	atexit(glfwTerminate);

	// Creamos la ventana
	glfwWindowHint(GLFW_RESIZABLE, false);
	GLFWwindow* window = glfwCreateWindow(1000, 800, "Practica Miguel Franco Medina", nullptr, nullptr);

	if (!window) {
		cout << "Error: No se ha podido crear la ventana" << endl;
		return -1;
	}

	// Activamos contexto de OpenGL
	glfwMakeContextCurrent(window);

	// Inicializamos LiteGFX
	lgfx_setup2d(1000, 800);
	lgfx_setresolution(1000, 800);

	// Cargamos imagenes
	ltex_t* texBee	  = loadTexture("../data/bee_anim.png");
	ltex_t* texLevel  = loadTexture("../data/level.png");
	ltex_t* texTrees1 = loadTexture("../data/trees1.png");
	ltex_t* texTrees2 = loadTexture("../data/trees2.png");
	ltex_t* texClouds = loadTexture("../data/clouds.png");

	if (!texBee || !texLevel || !texTrees1 || !texTrees2 || !texClouds) {
		std::cout << "No se ha podido cargar la textura" << std::endl;
		return -1;
	}

	// Declaramos variable de posicion del raton
	Vec2 mousePos;

	// Creamos sprite bee
	Sprite *spriteBee = new Sprite(texBee, 8);
	spriteBee->setCallback(updateBee);
	spriteBee->setUserData(&mousePos);
	spriteBee->setFps(8);
	spriteBee->setWeight(WEIGHT_START);
	spriteBee->setColor(static_cast<float>(rand() % 255) / 255.f, static_cast<float>(rand() % 255) / 255.f, static_cast<float>(rand() % 255) / 255.f, 1.f);

	// Creamos el mundo con sus propiedades.
	World world(0.15f, 0.15f, 0.45f, texClouds, texTrees2, texTrees1, texLevel);
	world.setScrollRatio(3, 1.0f);
	world.setScrollRatio(2, 0.8f);
	world.setScrollRatio(1, 0.6f);
	world.setScrollRatio(0, 0.4f);
	world.setScrollSpeed(0, Vec2(-16, -8));
	world.addSprite(spriteBee);

	// Bucle principal
	double lastTime = glfwGetTime();
	while (!glfwWindowShouldClose(window) && !glfwGetKey(window, GLFW_KEY_ESCAPE)) {
		// Pintado
		// Actualizamos delta
		float deltaTime = static_cast<float>(glfwGetTime() - lastTime);
		lastTime = glfwGetTime();

		// Actualizamos tama�o de ventana
		int screenWidth, screenHeight;
		glfwGetWindowSize(window, &screenWidth, &screenHeight);
		lgfx_setviewport(0, 0, screenWidth, screenHeight);
		Vec2 screenSize(static_cast<float>(screenWidth), static_cast<float>(screenHeight));

		// Actualizamos coordenadas de raton y la camara
		double mouseX, mouseY;
		glfwGetCursorPos(window, &mouseX, &mouseY);
		if (glfwGetWindowAttrib(window, GLFW_FOCUSED)) {
			mousePos = Vec2(static_cast<float>(mouseX + world.getCameraPosition().x), static_cast<float>(mouseY + world.getCameraPosition().y));

			// Establecemos los l�mites del mapa para que no pueda salir
			if (mousePos.x < 0) mousePos.x = 0;
			else if (mousePos.x > world.getBackground(3)->width) mousePos.x = world.getBackground(3)->width;

			if (mousePos.y < 0) mousePos.y = 0;
			else if (mousePos.y > world.getBackground(3)->height) mousePos.y = world.getBackground(3)->height;
		}

		// Pintado
		world.update(deltaTime);
		world.draw(Vec2(screenWidth, screenHeight));

		// Pintado de las bolas // TODO: Mejorar funcion de pintado
		for (int i = 0; i < listBallsLife.size(); i++) {
			lgfx_setcolor(listBallsLife[i].mR, listBallsLife[i].mG, listBallsLife[i].mB, 1.f);
			lgfx_drawoval(listBallsLife[i].mPos.x, listBallsLife[i].mPos.y, listBallsLife[i].mWeight, listBallsLife[i].mWeight);
		}

		// Actualizamos ventana y eventos
		glfwSwapBuffers(window);
		glfwPollEvents();

		// Cliente
		std::vector<CPacketENet*>  incommingPackets;
		Vec2 *vec;

		NetMessage				newMessage;
		NetMessageSnapShot		snapshotMessage;
		NetMessageChangePos		newMessagePos;
		NetMessageUserStartGame userStart;
		NetMessageBallLifeEaten ballLifeEaten;
		SnapshotInfo			info;
		NetMessageUserEaten     userEatenMessage;
		NetMessageDeleteUser    deleteUserMessage;

		char *newBuffer;

		pClient->Service(incommingPackets, 0);
		for (int i = 0; i < incommingPackets.size(); i++) {
			switch (incommingPackets[i]->GetType()) {
				case DATA:
					newBuffer = reinterpret_cast<char *>(incommingPackets[i]->GetData());
					newMessage.deserialize(newBuffer);

					switch (newMessage.mType) {
						case NETMSG_CHANGEPOS:
							newMessagePos = NetMessageChangePos();
							newMessagePos.deserialize(newBuffer);

							for (int i = 0; i < listUsers.size(); i++) {
								if (listUsers[i]->mId != -1 && listUsers[i]->mId == newMessagePos.mData.mId) {
									listUsers[i]->mPos.x = newMessagePos.mData.mX;
									listUsers[i]->mPos.y = newMessagePos.mData.mY;
									break;
								}
							}

							for (int i = 0; i < world.mListSprites.size(); i++) {
								if (world.mListSprites[i]->getId() != -1 && world.mListSprites[i]->getId() == newMessagePos.mData.mId) {
									world.mListSprites[i]->setMousePos(Vec2(newMessagePos.mData.mX, newMessagePos.mData.mY));
									break;
								}
							}

							break;
						case NETMSG_USERSTARTGAME:
							cout << "NETMSG_USERSTARTGAME" << endl;
							userStart = NetMessageUserStartGame();
							userStart.deserialize(newBuffer);

							mPlayer->mId     = userStart.mData.mId;
							mPlayer->mWeight = WEIGHT_START;
							world.mListSprites[0]->setId(mPlayer->mId);
							world.mListSprites[0]->setWeight(WEIGHT_START);

							break;
						case NETMSG_SNAPSHOT:
							snapshotMessage = NetMessageSnapShot();
							snapshotMessage.deserialize(newBuffer);
							info = snapshotMessage.mData;

							// TODO: Meter en una funcion
							for (int i = 0; i < info.mNumPlayers; i++) {
								if (mPlayer->mId == info.mUsersInfo[i].mId) {
									/*mPlayer->mPos.x  = info.mUsersInfo[i].mX;
									mPlayer->mPos.y  = info.mUsersInfo[i].mY;
									mPlayer->mWeight = info.mUsersInfo[i].mWeight;*/
								} else {
									bool exist = false;
									for (int j = 0; j < listUsers.size(); j++) {
										if (listUsers[j]->mId == info.mUsersInfo[i].mId) {
											listUsers[j]->mPos.x  = info.mUsersInfo[i].mX;
											listUsers[j]->mPos.y  = info.mUsersInfo[i].mY;
											listUsers[j]->mSpeed  = info.mUsersInfo[i].mSpeed;
											listUsers[j]->mWeight = info.mUsersInfo[i].mWeight;

											exist = true;
											break;
										}
									}
									if (!exist) {
										// A�adir jugador
										listUsers.push_back(new Client(info.mUsersInfo[i].mId));
										// Creamos sprite bee
										Sprite *sprite = new Sprite(texBee, 8);
										sprite->setCallback(updateBee);
										sprite->setFps(8);
										sprite->setId(info.mUsersInfo[i].mId);
										sprite->setWeight(info.mUsersInfo[i].mWeight);
										sprite->setColor(static_cast<float>(rand() % 255) / 255.f, static_cast<float>(rand() % 255) / 255.f, static_cast<float>(rand() % 255) / 255.f, 1.f);

										// A�adimos el sprite al mundo
										world.addSprite(sprite);
									}
								}

								// Actualizamos posicion de los sprites
								for (int j = 0; j < world.mListSprites.size(); j++) {
									if (world.mListSprites[j]->getId() == info.mUsersInfo[i].mId && info.mUsersInfo[i].mId != mPlayer->mId) {
										world.mListSprites[j]->setMousePos(Vec2(info.mUsersInfo[i].mX, info.mUsersInfo[i].mY));
										break;
									}
								}
							}

							//listBallsLife.clear(); // TODO: Mejorar como a�adir bolas
							for (int i = 0; i < info.mNumBallsLife; i++) {
								bool found = false;
								for (int j = 0; j < listBallsLife.size(); j++) {
									if (listBallsLife[j].mId == info.mBallsInfo[i].mId) {
										listBallsLife[j].mPos.x = info.mBallsInfo[i].mX;
										listBallsLife[j].mPos.y = info.mBallsInfo[i].mY;
										found = true;
										break;
									}
								}

								// Si no se encuentra se a�ade.
								if (!found) {
									listBallsLife.push_back(BallLife(Vec2(info.mBallsInfo[i].mX, info.mBallsInfo[i].mY), info.mBallsInfo[i].mId, info.mBallsInfo[i].mR, info.mBallsInfo[i].mG, info.mBallsInfo[i].mB, info.mBallsInfo[i].mWeight));
								}
							}

							break;
						case NETMSG_BALLEATEN:
							cout << "BALL EATEN" << endl;
							ballLifeEaten = NetMessageBallLifeEaten();
							ballLifeEaten.deserialize(newBuffer);

							for (int i = 0; i < listBallsLife.size(); i++) {
								if (listBallsLife[i].mId == ballLifeEaten.mData.mId) {
									listBallsLife.erase(listBallsLife.begin() + i);
									break;
								}
							}

							if (mPlayer->mId == ballLifeEaten.mData.mIdUser) {
								mPlayer->mWeight = ballLifeEaten.mData.mWeight;
								world.mListSprites[0]->setWeight(mPlayer->mWeight);
							} else {
								for (int i = 0; i < listUsers.size(); i++) {
									if (listUsers[i]->mId == ballLifeEaten.mData.mIdUser) {
										listUsers[i]->mWeight = ballLifeEaten.mData.mWeight;
										for (int j = 0; j < world.mListSprites.size(); j++) {
											if (world.mListSprites[j]->getId() == listUsers[i]->mId) {
												world.mListSprites[j]->setWeight(listUsers[i]->mWeight);
												break;
											}
										}
										break;
									}
								}
							}

							break;
						case NETMSG_USEREATEN:
							cout << "USER EATEN" << endl;
							userEatenMessage = NetMessageUserEaten();
							userEatenMessage.deserialize(newBuffer);

							if (mPlayer->mId == userEatenMessage.mData.mIdUserEaten) {
								pClient->Disconnect(pPeer);
								return 0;
							} else {
								for (int i = 0; i < listUsers.size(); i++) {
									if (listUsers[i]->mId == userEatenMessage.mData.mIdUserEaten) {
										int id = listUsers[i]->mId;
										listUsers.erase(listUsers.begin() + i);
										for (int j = 0; j < world.mListSprites.size(); j++) {
											if (world.mListSprites[j]->getId() == id) {
												world.mListSprites.erase(world.mListSprites.begin() + j);
												break;
											}
										}
										break;
									}
								}

								if (mPlayer->mId == userEatenMessage.mData.mIdUser) {
									mPlayer->mWeight = userEatenMessage.mData.mWeight;
									world.mListSprites[0]->setWeight(mPlayer->mWeight);
								} else {
									for (int i = 0; i < listUsers.size(); i++) {
										if (listUsers[i]->mId == userEatenMessage.mData.mIdUser) {
											listUsers[i]->mWeight = userEatenMessage.mData.mWeight;
											for (int j = 0; j < world.mListSprites.size(); j++) {
												if (world.mListSprites[j]->getId() == listUsers[i]->mId) {
													world.mListSprites[j]->setWeight(listUsers[i]->mWeight);
													break;
												}
											}
											break;
										}
									}
								}
							}

							break;
						case NETMSG_DELETEUSER:
							cout << "USER DELETE" << endl;
							deleteUserMessage = NetMessageDeleteUser();
							deleteUserMessage.deserialize(newBuffer);

							for (int i = 0; i < listUsers.size(); i++) {
								if (listUsers[i]->mId == deleteUserMessage.mData.mId) {
									int id = deleteUserMessage.mData.mId;
									listUsers.erase(listUsers.begin() + i);

									for (int j = 0; j < world.mListSprites.size(); j++) {
										if (world.mListSprites[j]->getId() == id) {
											world.mListSprites.erase(world.mListSprites.begin() + j);
											break;
										}
									}
									break;
								}
							}
							
							break;
						default:
							cout << "DEFAULT TYPE" << endl;
							break;
					}

					free(newBuffer);
					break;
				case DISCONNECT:
					cout << "disconnet" << endl;
					return 0;
					break;
				default:
					cout << "default" << endl;
					break;
			}
		}

		// Enviamos por mensage Net actualizando la posicion del jugador
		NetMessageChangePos messagePos(ChangePosUserInfo(mPlayer->mPos.x, mPlayer->mPos.y, mPlayer->mId));
		char* bufferChangePos = (char*)malloc(32);
		int sizeMessage = messagePos.serialize(bufferChangePos);
		pClient->SendData(pPeer, bufferChangePos, sizeMessage, 0, true);
		free(bufferChangePos);

		Sleep(10);
    }

    pClient->Disconnect(pPeer);
	
	// Eliminamos textura
	ltex_free(texBee);

    return 0;
}

void updateBee(Sprite& sprite, float deltaTime) {
	// Obtenemos mouse pos
	Vec2 mousePos;
	if (mPlayer->mId == sprite.getId()) {
		mousePos = *reinterpret_cast<Vec2*>(sprite.getUserData());
	}

	// Movemos sprite
	Vec2 directorVec = mousePos - sprite.getPosition();
	Vec2 destDir;
	if (directorVec.length() > 1 && mPlayer->mId == sprite.getId()) {
		destDir = directorVec.norm();
		float speed = (static_cast<float>(static_cast<float>(SPEED_START) * 2.5f / static_cast<float>(mPlayer->mWeight)) * 300.f);
		sprite.setPosition(sprite.getPosition() + destDir * speed * deltaTime);
		if (mPlayer->mId == sprite.getId()) {
			mPlayer->mPos = sprite.getPosition();
		}
	}

	for (int i = 0; i < listUsers.size(); ++i) {
		if (listUsers[i]->mId == sprite.getId()) {
			sprite.setPosition(listUsers[i]->mPos);
		}
	}

	// Encontramos angulo de destino
	//float destAngle = sprite.getAngle();
	//if (destDir.x > 0)
	//	destAngle = -15;
	//else if (destDir.x < 0)
	//	destAngle = 15;
	//else if (destDir.y == 0)
	//	destAngle = 0;

	//// Rotamos sprite
	//if (destAngle > sprite.getAngle()) {
	//	sprite.setAngle(sprite.getAngle() + 32 * deltaTime);
	//	if (sprite.getAngle() > destAngle) sprite.setAngle(destAngle);
	//}
	//else {
	//	sprite.setAngle(sprite.getAngle() - 32 * deltaTime);
	//	if (sprite.getAngle() < destAngle) sprite.setAngle(destAngle);
	//}
}
