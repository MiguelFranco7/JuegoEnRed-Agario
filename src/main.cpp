//#ifdef _MSC_VER
//#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
//#endif
//
//#include "ugine.h"
//#include <glfw3.h>
//#include <iostream>
//#include <vector>
//
//using namespace std;
//
//void updateBee(Sprite& sprite, float deltaTime) {
//	// Obtenemos mouse pos
//	const Vec2& mousePos = *reinterpret_cast<Vec2*>(sprite.getUserData());
//
//	// Movemos sprite
//	Vec2 directorVec = mousePos - sprite.getPosition();
//	Vec2 destDir;
//	if (directorVec.length() > 1) {
//		destDir = directorVec.norm();
//		sprite.setPosition(sprite.getPosition() + destDir * 300.0f * deltaTime);
//	}
//
//	// Encontramos angulo de destino
//	float destAngle = sprite.getAngle();
//	if (destDir.x > 0)
//		destAngle = -15;
//	else if (destDir.x < 0)
//		destAngle = 15;
//	else if (destDir.y == 0)
//		destAngle = 0;
//
//	// Rotamos sprite
//	if (destAngle > sprite.getAngle()) {
//		sprite.setAngle(sprite.getAngle() + 32 * deltaTime);
//		if (sprite.getAngle() > destAngle) sprite.setAngle(destAngle);
//	}
//	else {
//		sprite.setAngle(sprite.getAngle() - 32 * deltaTime);
//		if (sprite.getAngle() < destAngle) sprite.setAngle(destAngle);
//	}
//}
//
//int main() {
//	// Inicializamos GLFW
//	if (!glfwInit()) {
//		cout << "Error: No se ha podido inicializar GLFW" << endl;
//		return -1;
//	}
//	atexit(glfwTerminate);
//
//	// Creamos la ventana
//	glfwWindowHint(GLFW_RESIZABLE, false);
//	GLFWwindow* window = glfwCreateWindow(800, 600, "Practica 6 Miguel Franco Medina", nullptr, nullptr);
//	
//	if (!window) {
//		cout << "Error: No se ha podido crear la ventana" << endl;
//		return -1;
//	}
//
//	// Activamos contexto de OpenGL
//	glfwMakeContextCurrent(window);
//
//	// Inicializamos LiteGFX
//	lgfx_setup2d(800, 600);
//	lgfx_setresolution(800, 600);
//
//	// Cargamos imagenes
//	ltex_t* texBee	  = loadTexture("../data/bee_anim.png");
//	ltex_t* texLevel  = loadTexture("../data/level.png");
//	ltex_t* texTrees1 = loadTexture("../data/trees1.png");
//	ltex_t* texTrees2 = loadTexture("../data/trees2.png");
//	ltex_t* texClouds = loadTexture("../data/clouds.png");
//
//	if (!texBee || !texLevel || !texTrees1 || !texTrees2 || !texClouds) {
//		std::cout << "No se ha podido cargar la textura" << std::endl;
//		return -1;
//	}
//
//	// Declaramos variable de posicion del raton
//	Vec2 mousePos;
//
//	// Creamos sprite bee
//	Sprite spriteBee(texBee, 8);
//	spriteBee.setCallback(updateBee);
//	spriteBee.setUserData(&mousePos);
//	spriteBee.setFps(8);
//
//	// Creamos el mundo con sus propiedades.
//	World world(0.15f, 0.15f, 0.45f, texClouds, texTrees2, texTrees1, texLevel);
//	world.setScrollRatio(3, 1.0f);
//	world.setScrollRatio(2, 0.8f);
//	world.setScrollRatio(1, 0.6f);
//	world.setScrollRatio(0, 0.4f);
//	world.setScrollSpeed(0, Vec2(-16, -8));
//	world.addSprite(spriteBee);
//
//	// Bucle principal
//	double lastTime = glfwGetTime();
//	while (!glfwWindowShouldClose(window) && !glfwGetKey(window, GLFW_KEY_ESCAPE)) {
//		// Actualizamos delta
//		float deltaTime = static_cast<float>(glfwGetTime() - lastTime);
//		lastTime = glfwGetTime();
//
//		// Actualizamos tama�o de ventana
//		int screenWidth, screenHeight;
//		glfwGetWindowSize(window, &screenWidth, &screenHeight);
//		lgfx_setviewport(0, 0, screenWidth, screenHeight);
//		Vec2 screenSize(static_cast<float>(screenWidth), static_cast<float>(screenHeight));
//
//		// Actualizamos coordenadas de raton y la camara
//		double mouseX, mouseY;
//		glfwGetCursorPos(window, &mouseX, &mouseY);
//		mousePos = Vec2(static_cast<float>(mouseX + world.getCameraPosition().x), static_cast<float>(mouseY + world.getCameraPosition().y));
//
//		// Pintado
//		world.update(deltaTime);
//		world.draw(Vec2(screenWidth, screenHeight));
//
//		// Actualizamos ventana y eventos
//		glfwSwapBuffers(window);
//		glfwPollEvents();
//	}
//
//	// eliminamos textura
//	ltex_free(texBee);
//
//    return 0;
//}
