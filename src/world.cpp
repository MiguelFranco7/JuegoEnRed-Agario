#include "world.h"

void World::update(float deltaTime) {
	// Actualizar valor del scroll autom�tico en base a la velocidad establecida para cada fondo con setScrollSpeed.
	for (size_t i = 0; i < mBacks.size(); i++) {
		mBacksPos[i].x += mVelScrollBacks[i].x * deltaTime;
		mBacksPos[i].y += mVelScrollBacks[i].y * deltaTime;
	}
	
	// Llamar al m�todo update de cada sprite del mundo.
	for (size_t i = 0; i < mListSprites.size(); i++)
		mListSprites[i]->update(deltaTime);
}

void World::draw(const Vec2 & screenSize) {
	// Borrar fondo con el color especificado.
	lgfx_clearcolorbuffer(mColorClear[0], mColorClear[1], mColorClear[2]);
	lgfx_setorigin(0, 0);

	// Actualizado de la camara.
	Vec2 cameraPos = Vec2(mListSprites[0]->getPosition().x - screenSize.x / 2, mListSprites[0]->getPosition().y - screenSize.y / 2);
	if (cameraPos.y < 0)
		cameraPos.y = 0;

	if (cameraPos.y + screenSize.y > mBacks[3]->height)
		cameraPos.y = mBacks[3]->height - screenSize.y;

	if (cameraPos.x < 0)
		cameraPos.x = 0;

	if (cameraPos.x + screenSize.x > mBacks[3]->width)
		cameraPos.x = mBacks[3]->width - screenSize.x;

	for (size_t i = 0; i < mBacks.size(); i++) {
		float stepX = screenSize.x / static_cast<float>(mBacks[i]->width);
		float stepY = screenSize.y / static_cast<float>(mBacks[i]->height);

		float u0 = ((cameraPos.x * mRatiosScrollBacks[i] - mBacksPos[i].x) / static_cast<float>(mBacks[i]->width)) ;
		float v0 = ((cameraPos.y * mRatiosScrollBacks[i] - mBacksPos[i].y) / static_cast<float>(mBacks[i]->height));

		float u1 = u0 + stepX;
		float v1 = v0 + stepY;

		ltex_drawrotsized(mBacks[i], 0, 0, 0, 0, 0, static_cast<float>(screenSize.x), static_cast<float>(screenSize.y), u0, v0, u1, v1);
	}
	
	// Pintar los sprites aplicar la traslaci�n de la c�mara previamente con lgfx_setorigin.
	lgfx_setorigin(cameraPos.x, cameraPos.y);
	setCameraPosition(cameraPos);

	for (size_t i = 0; i < mListSprites.size(); i++)
		mListSprites[i]->draw();
}
