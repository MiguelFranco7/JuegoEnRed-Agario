#pragma once

class Vec2;

class BallLife {
public:
	BallLife(Vec2 pos, int id, float r, float g, float b, int weight) { mPos = pos; mId = id; mR = r; mG = g, mB = b; mWeight = weight; }
	BallLife(Vec2 pos) { mPos = pos; }

	Vec2   mPos;
	int	   mId = -1;
	float  mR;
	float  mG;
	float  mB;
	int    mWeight;
};