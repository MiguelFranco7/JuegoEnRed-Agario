#pragma once

#include "../EnetTest/EnetWrapper/ClientENet.h"
#include "../Config/GameConfig.h"

class Vec2;

using namespace ENet;

class User {
public:
	User(Vec2 pos, CPeerENet *peer, int id) { mPos = pos; mPeer = peer; mId = id; }
	User(Vec2 pos, CPeerENet *peer)         { mPos = pos; mPeer = peer; }

	Vec2	   mPos;
	CPeerENet *mPeer;
	int		   mId	   = -1;
	int		   mWeight = WEIGHT_START;
	float	   mSpeed  = SPEED_START;
};
