#pragma once

#include "vec2.h"
#include <litegfx.h>
#include <array>

class Sprite {
public:
	typedef void(*CallbackFunc)(Sprite&, float);

	// Indicamos el n�mero de frames en horizontal y vertical
	// que tendr� la imagen del sprite
	Sprite(const ltex_t* tex, int hframes = 1, int vframes = 1) :
		mCallback(nullptr),
		mTexture(tex),
		mHframes(hframes),
		mVframes(vframes),
		mBlend(BLEND_ALPHA),
		mColor{1,1,1,1},
		mPosition(1, 1),
		mAngle(0),
		mScale(1, 1),
		mPivot(0.5f, 0.5f),
		mFps(0),
		mCurrentFrame(0) {}

	const ltex_t* getTexture() const { return mTexture; }
	void setTexture(const ltex_t* tex) { mTexture = tex; }

	lblend_t getBlend() const { return mBlend; }
	void setBlend(lblend_t mode) { mBlend = mode; }

	float getRed() const { return mColor[0]; }
	float getGreen() const { return mColor[1]; }
	float getBlue() const { return mColor[2]; }
	float getAlpha() const { return mColor[3]; }
	void setColor(float r, float g, float b, float a) { mColor[0] = r; mColor[1] = g; mColor[2] = b; mColor[3] = a; }

	const Vec2& getPosition() const { return mPosition; }
	void setPosition(const Vec2& pos) { mPosition = pos; }

	const Vec2& getMousePos() const { return mMousePos; }
	void setMousePos(const Vec2& pos) { mMousePos = pos; }

	float getAngle() const { return mAngle; }
	void setAngle(float angle) { mAngle = angle; }

	const Vec2& getScale() const { return mScale; }
	void setScale(const Vec2& scale) { mScale = scale; }

	// Tama�o de un frame multiplicado por la escala
	Vec2 getSize() const { return Vec2(static_cast<float>(mTexture->width / mHframes), static_cast<float>(mTexture->height / mVframes)) * mScale; }

	// Este valor se pasa a ltex_drawrotsized en el pintado
	// para indicar el pivote de rotaci�n
	const Vec2& getPivot() const { return mPivot; }
	void setPivot(const Vec2& pivot) { mPivot = pivot; }

	int getHframes() const { return mHframes; }
	int getVframes() const { return mVframes; }
	void setFrames(int hframes, int vframes) { mHframes = hframes; mVframes = vframes; }

	// Veces por segundo que se cambia el frame de animaci�n
	int getFps() const { return mFps; }
	void setFps(int fps) { mFps = fps; }

	// Frame actual de animaci�n
	float getCurrentFrame() const { return mCurrentFrame; }
	void setCurrentFrame(int frame) { mCurrentFrame = static_cast<float>(frame); }

	void update(float deltaTime);
	void draw() const;

	CallbackFunc getCallback() { return mCallback; }
	void setCallback(CallbackFunc callback) { mCallback = callback; }

	void* getUserData() { return mUserData; }
	void setUserData(void* data) { mUserData = data; }

	bool operator==(Sprite &other) const { return  mTexture == other.mTexture && mPosition == other.mPosition; }

	int getId() const { return mId; }
	void setId(int id) { mId = id; }

	int  getWeight() const { return mWeight; }
	void setWeight(int weight) { mWeight = weight; }

private:
	const ltex_t*			mTexture;
	int						mHframes;
	int						mVframes;
	lblend_t				mBlend;
	std::array<float, 4>	mColor;
	Vec2					mPosition;
	Vec2					mMousePos;
	float					mAngle;
	Vec2					mScale;
	Vec2					mPivot;
	int						mFps;
	float					mCurrentFrame;
	CallbackFunc			mCallback;
	void*					mUserData;
	int						mId;
	int						mWeight;
};
